import { REFRESH_TOKEN } from "./types";
  
export const refreshToken = (accessToken) => (dispatch) => {
    dispatch({
        type: REFRESH_TOKEN,
        payload: accessToken
    })
}