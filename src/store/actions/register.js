import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    SET_MESSAGE
} from "./types";
import AuthService from "../../services/auth.service";

export const register = (firstName, lastName, email, password) => (dispatch) => {
    console.log("register");
    console.log(firstName, lastName, email);
    return AuthService.register(firstName, lastName, email, password).then(
      (response) => {
        dispatch({
          type: REGISTER_SUCCESS,
        });
  
        dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });
  
        return Promise.resolve();
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
  
        dispatch({
          type: REGISTER_FAIL,
        });
  
        dispatch({
          type: SET_MESSAGE,
          payload: message,
        });
  
        return Promise.reject();
      }
    );
  };