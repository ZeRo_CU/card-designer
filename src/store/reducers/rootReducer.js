import { combineReducers } from 'redux';
import login from './login';
import register from './register';
import message from './message';

const reducer = combineReducers({
    login,
    register,
    message
});

export default reducer;