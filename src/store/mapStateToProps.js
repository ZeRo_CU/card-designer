function mapStateToProps(component) {
	switch (component) {
		case "Login": {
			return function (state) {
				const { isLoggedIn } = state.login;
				const { message } = state.message;
				return {
					isLoggedIn,
					message
				};
			}
		}
		case "Register": {
			return function (state) {
				const { message } = state.message;
				return {
					message
				};
			}
		}
		default: return undefined;
	}
}

export default mapStateToProps;