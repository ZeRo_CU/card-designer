import * as React from 'react';
import { Button, ButtonGroup} from '@material-ui/core';

export default function DataOptionPicker() {
  return (
    <ButtonGroup variant="contained" size="large" aria-label="outlined button group">
        <Button>Google Design</Button>
        <Button>Apple Design</Button>
        <Button>Barcode / NFC</Button>
    </ButtonGroup>
  );
}