import { Link as RouterLink } from 'react-router-dom';
import { 
  AppBar,
  Toolbar,
  Box,
  Button
 } from '@material-ui/core';
import {
  CreditCard as CreditCardIcon
} from 'react-feather';
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
  whiteLink: {
    color: "white"
  }
}));

const MainNavbar = (props) => {
  const classes = useStyles();

  return (
    <AppBar
      elevation={0}
      {...props}
    >
      <Toolbar sx={{ height: 64 }}>
        <RouterLink to="/">
          <CreditCardIcon color="#FFFFFF" />
        </RouterLink>
        <Box sx={{ flexGrow: 1 }} />
        <Button className={classes.whiteLink} href="/login">Log in</Button>
      </Toolbar>
    </AppBar>
  );
};

export default MainNavbar;
