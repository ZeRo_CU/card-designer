import Dashboard from 'pages/Dashboard';
import { Navigate } from 'react-router-dom';
import MainLayout from './components/MainLayout';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import Register from './pages/Register';

const routes = [
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'login', element: <Login /> },
      { path: 'register', element: <Register /> },
      { path: '404', element: <NotFound /> },
      { path: '/', element: <Dashboard /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
