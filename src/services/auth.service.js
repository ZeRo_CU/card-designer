import api from "./api.service";
import TokenService from "./token.service";
import { API_URL } from "constants/api"

class AuthService {
  login(email, password) {
    console.log("login: " + email + " " + password);
    return api
      .post(API_URL + "login", { email, password })
      .then((response) => {
        if (response.data.accessToken) {
          TokenService.setUser(response.data)
        }

        return response.data;
      });
  }

  logout() {
    TokenService.removeUser();
  }

  register(firstName, lastName, email, password) {
    return api
      .post(API_URL + "register", {
        firstName,
        lastName,
        email,
        password,
      });
  }
}

export default new AuthService();