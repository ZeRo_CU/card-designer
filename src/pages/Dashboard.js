import { Helmet } from 'react-helmet';
import { useState } from 'react';
import {
  Box,
  Container,
  Divider,
  Grid
} from '@material-ui/core';
import DashboardSidebar from 'components/DashboardSidebar';
import mapStateToProps from 'store/mapStateToProps'
import mapDispatchToProps from 'store/mapDispatchToProps'
import { connect } from 'react-redux';
import DemoCard from 'components/DemoCard';
import DataOptionPicker from 'components/DataOptionPicker';
import AppleDataOptionsList from 'components/DataOptionLists/AppleDataOptionsList';

const Dashboard = (props) => {
    const [isMobileNavOpen, setMobileNavOpen] = useState(false);
    console.log(props)
    return ( 
    <>
        <Helmet>
            <title>Dashboard</title>
        </Helmet>
        <Box
        sx={{
            backgroundColor: 'background.default',
            minHeight: '100%',
            py: 3
        }}
        >
        <Container maxWidth={false}>
            <Grid container spacing={2}>
                <Grid item xs={2}>
                    <DashboardSidebar
                        onMobileClose={() => setMobileNavOpen(false)}
                        openMobile={isMobileNavOpen}>
                            <Divider />
                            <AppleDataOptionsList />
                    </DashboardSidebar>
                </Grid>
                <Grid item xs={5}>
                    <DataOptionPicker />
                </Grid>
                <Grid item xs={5}>
                    <DemoCard />
                </Grid>
            </Grid>
        </Container>
        </Box>
    </>
  );
};

const DASHBOARD_W = connect(mapStateToProps("Dashboard"), mapDispatchToProps("Dashboard"))(Dashboard);

export default DASHBOARD_W;
